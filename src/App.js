import './App.css';
import BasicForm from './components/basicForm';
import { Count } from './components/count';

function App() {
  return (
    <div style = {{margin: '50px'}}>
      <p>Task 519.10</p>
      <Count/>
      <hr/>
      <p>Task 519.20</p>
      <BasicForm/>
    </div>
  );
}

export default App;
