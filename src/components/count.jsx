import { useEffect, useState } from "react"

export const Count = () => {
    const [count, setCount] =  useState(0)

    const ClickCount = () => {
        setCount(count + 1)
    }

    useEffect(() => {
        console.log('Hook 1 call')

    },[])
    useEffect(() => {
        console.log('Hook 2 call')

    })
     // Component did mount + Component did update + count update
     useEffect(() => {
        console.log("Hook3 called!");
    }, [count]);

    return (
        <>
            <p>
                You Click {count} time
            </p>
            <button onClick={ClickCount}>Click Me</button>
        </>
    )
}