import { useEffect, useState } from "react"

const BasicForm = () => {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName") || "")
    const [lastName, setLastName] = useState(localStorage.getItem("lastName") || "")

    const onInputFirstNameChangeHandler = (event) => {
        setFirstName(event.target.value);
    }

    const onInputLastNameChangeHandler = (event) => {
        setLastName(event.target.value);
    }

    useEffect (() => {
        localStorage.setItem("firstName", firstName)
        localStorage.setItem("lastName", lastName)
    })
    return(
        <div>
            <input value={firstName} onChange={onInputFirstNameChangeHandler}></input>
            <br/>
            <input value={lastName} onChange={onInputLastNameChangeHandler}></input>
            <br/>
            <p>{lastName} {firstName}</p>
        </div>

    )
}

export default BasicForm    